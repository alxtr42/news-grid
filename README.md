# news-grid

Repository for a news-grid Website example using css grid

You can check out live version here: (https://news-grid-website.netlify.com/)
# NewsGrid Website

Basic Website example using css grid

[Alex T.](https://alxtr42.github.io/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

1. Clone the repo

```
git clone https://alxtr42@bitbucket.org/alxtr42/news-grid.git
```

2. Open home file

```
open index.html
```

3. Explore

```
Enjoy :)
```

## Built With

* HTML5
* CSS

## Contributors

* **Aleksandar T.** - *Initial work* - [Alex T.](https://alxtr42.github.io/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Font Awesome](https://fontawesome.com/?from=io/) - Icons used




